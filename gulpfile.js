/******************************************************
 * PATTERN LAB NODE
 * EDITION-NODE-GULP
 * The gulp wrapper around patternlab-node core, providing tasks to interact with the core library.
******************************************************/
const gulp = require('gulp'),
      argv = require('minimist')(process.argv.slice(2));

var path = require('path'),
    sass = require('gulp-sass'),
    gulpStylelint = require('gulp-stylelint'),
    rename = require('gulp-rename'),
    cssmin = require('gulp-cssnano'),
    concat = require('gulp-concat'),
    prefix = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    iconfont = require('gulp-iconfont'),
    iconfontCss = require('gulp-iconfont-css'),
    eslint = require('gulp-eslint'),
    gulpSequence = require('gulp-sequence'),
    fontName = 'icons',
    prefixerOptions = {
      overrideBrowserslist: [
        "last 4 version",
        "> 1%",
        "IE 10"
      ]
    };
    
    sass.compiler = require('node-sass');


/******************************************************
 * PATTERN LAB  NODE WRAPPER TASKS with core library
******************************************************/
const config = require('./patternlab-config.json');
const patternlab = require('@pattern-lab/patternlab-node')(config);

function build() {
  return patternlab.build({
    watch: argv.watch,
    cleanPublic: config.cleanPublic
  }).then(() =>{
    // do something else when this promise resolves
  });
}

function serve() {
  return patternlab.serve({
    cleanPublic: config.cleanPublic
  }
).then(() => {
  // do something else when this promise resolves
  });
}

gulp.task('patternlab:version', function () {
  patternlab.version();
});

gulp.task('patternlab:help', function () {
  patternlab.help();
});

gulp.task('patternlab:patternsonly', function () {
  patternlab.patternsonly(config.cleanPublic);
});

gulp.task('patternlab:liststarterkits', function () {
  patternlab.liststarterkits();
});

gulp.task('patternlab:loadstarterkit', function () {
  patternlab.loadstarterkit(argv.kit, argv.clean);
});

gulp.task('patternlab:build', function () {
  build().then(() => {
    // do something else when this promise resolves
  });
});

gulp.task('patternlab:serve', function () {
  serve().then(() => {
    
  });
});

gulp.task('patternlab:installplugin', function () {
  patternlab.installplugin(argv.plugin);
});

//Eslint js files 
gulp.task('eslint', () => {
  return gulp.src('source/js/**/nav.js')
  .pipe(eslint())
  .pipe(eslint.format())
  .pipe(eslint.failAfterError())
});

//Minify and Concat .scss files
gulp.task('sass', function () {
  return gulp.src('source/css/**/style.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(prefix(prefixerOptions))
    .pipe(rename('style.css'))
    .pipe(cssmin())
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest('public/css'));
});

//Minify and Concat js files 
gulp.task('js', function () {
  return gulp.src('source/js/**/*.js')
    .pipe(concat('all.js'))
    .pipe(rename('all.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('public/js'));
});

//StyleLint scss files 
gulp.task('stylelint', function lintCssTask() {
  return gulp
    .src('source/css/**/_body.scss')
    .pipe(gulpStylelint({
      fix: true,
      failAfterError: true,
      reporters: [ {
        formatter: 'string',
        console: true
      } ],
      debug: true
    }))
});

//Iconfont : Convert svg files to scss using templates 
gulp.task('iconfont', (done) => {
  gulp.src(['source/fonts/icons/*.svg'])
    .pipe(iconfontCss({
      fontName: fontName,
      path: 'source/css/scss/lib/_icons_template.scss',
      targetPath: '_icons.scss',
      fontPath: 'fonts/'
    }))
    .pipe(iconfont({
      fontName: fontName,
      fontHeight:1001,
      normalize:true
     }))
    .pipe(gulp.dest('source/css/scss/lib/'));
	done();
});

//Watching : Watches scss and js changes 
gulp.task('watch:code', function (done) {
  gulp.watch('source/css/**/*.scss', ['sass']);
  gulp.watch('source/js/**/*.js', ['js']);
});

/******************************************************
 * PRODUCTION TASKS
******************************************************/
gulp.task('production', gulpSequence('iconfont', 'stylelint','eslint'));

gulp.task('dev', gulpSequence('js', 'sass','watch:code'));

/******************************************************
 * RUNNING PATTERNLAB TASKS
******************************************************/
gulp.task('default', ['patternlab:serve','watch:code']);

